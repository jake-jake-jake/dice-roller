(ns dice-roller.util
  (:require [dice-roller.roller :as r]
            [clojure.string :as str]
            [clojure.walk :as walk]))

(defn- keys-for-range [r]
  (let [[s e] (str/split (str r) #"[-–]" 2)
        s (Integer/parseInt s)
        e (if e (Integer/parseInt e) s)]
    (range s (inc e))))

(defn range-table [rows]
  (into {} (mapcat identity (for [[k v] rows]
                              (for [i (keys-for-range k)]
                                [i v])))))

(def memo-table (memoize range-table))

(defn roll-on-table [{:keys [command label table]}]
  (let [roll (r/roll-dice-command command)
        res  (get (memo-table table) (:value roll))]
    {label {:result res
            :roll roll}}))

(defn clean-results [m]
  (walk/prewalk (fn [i] (if (map? i) (dissoc i :roll ) i)) m))

(defn printable-results [m]
  (let [r (map (fn [[k v]] (if (vector? v)
                             [k (mapv printable-results v)]
                             [k (:result v)])) m)]
    (->> r
         )))

(defn cli-fns []
  (->> (all-ns)
       (filter #(str/starts-with? (str %) "dice-roller" ))
       (mapcat ns-publics)
       (map second)
       (filter #(contains? (meta %) ::cli-fn))))
