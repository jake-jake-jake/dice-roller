(ns dice-roller.une.npc
  (:require [dice-roller.une.constants :as constants]
            [dice-roller.util :refer [roll-on-table clean-results printable-results]]))

(defn- npc-type []
  (let [npc-noun (roll-on-table constants/npc-noun)
        npc-mod  (roll-on-table constants/npc-modifier)]
    (merge npc-mod
           npc-noun
           )))

(defn- npc-motivation []
  (let [mot-verb (roll-on-table constants/npc-motivation-verb)
        mot-noun (roll-on-table constants/npc-motivation-noun)]
    (merge mot-verb mot-noun)))

(defn npc []
  (let [motivations (into [] (repeatedly 3 npc-motivation))]
    (merge (npc-type)
           {"NPC Motivations" motivations})))
