(ns dice-roller.main
  (:require [clojure.pprint :refer [pprint]]
            [dice-roller.roller :as r]
            [dice-roller.load :as l]
            [dice-roller.util :as util]
            [clojure.string :as str]))

(def spec {:cmd     {:desc         "Command to execute. Default `roller`"
                     :coerce       :keyword
                     :alias        :c
                     :default-desc "roller"
                     :default      :roller}
           :log     {:desc "Why the dice were rolled"
                     :alias :l}
           :verbose {:desc "Verbosity of output. If false, returns only value, not roll data."
                     :coerce :boolean
                     :alias :v
                     :default false}})
(require '[babashka.process :as p])
(require '[babashka.cli :as cli])

(defn fzf [s]
  (let [s (if (string? s) s (str/join "\n" s))
        proc (p/process ["fzf" "-m"]
                        {:in s :err :inherit
                         :out :string})]
    (:out @proc)))

(defn fzf-and-run-func []
  (let [s (fzf (util/cli-fns))]
    ((eval (read-string s)))))

(defn run-command* [cmd arg]
  (case cmd
    :roller (r/roll-dice-command arg)
    :func   (fzf-and-run-func)))

(defn run-command [args {:keys [cmd verbose] :as opts}]
  (let [[arg & _args] args
        res (run-command* cmd arg)]
    res))

(defn -main [& _args]
  (let [{:keys [args opts]} (cli/parse-args *command-line-args* {:spec spec})]
    (println (run-command args opts))))
