(ns dice-roller.roller
  (:require [clojure.string :as str]))

(def DICEPATTERN #"(\d+)?(?:\s*)?d(\d+)(?:\s*)?([+-]\d+\b)?")

(defn roll-di [n]
  (->> (rand-int n)
       inc))

(defn do-roll [[match & groups]]
  (let [[mult di mod] (map (fn [x] (if x
                                     (Integer/parseInt x)
                                     0)) groups)
        mult (max mult 1)
        mod (if (nil? mod) 0 mod)]
    (->> (into [] (repeatedly mult (fn [] (roll-di di))))
         ((fn [x] {:rolls x :modifier mod :roll-command match :value (reduce + (conj x mod))})))))

(defn roll-dice-command [s]
  (some->> s
           str/trim
           (re-matches DICEPATTERN)
           do-roll))

(defn make-roller [s]
  (let [cmds (for [m (re-seq DICEPATTERN s)]
               (first m))
        fns (map (fn [x] (fn [] (roll-dice-command x))) cmds)]
    (fn [] (into [] (for [f fns]
                      (f))))))

(defn roll-4d6-drop-lowest []
  (let [roll (roll-dice-command "4d6")]
    (->> roll
         :rolls
         sort
         reverse
         (take 3)
         (apply +)
         (assoc roll :value))))

(comment
  (defn mothership-stat-shuffle []
    (shuffle [20 25 25 30]))

  (defn roll-funnel-character []
    [(mothership-stat-shuffle)
     (:rolls (roll-dice-command "d100"))
     (:rolls (roll-dice-command "d100"))
     (:rolls (roll-dice-command "d100"))
     (:rolls (roll-dice-command "d100"))]))
