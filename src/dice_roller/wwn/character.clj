(ns dice-roller.wwn.character
  (:require
   [dice-roller.wwn.constants :as constants]
   [clojure.string :as str]
   [dice-roller.roller :as r]))


(defn- stat->key [s]
  (-> s (subs 0 3) str/lower-case keyword))

(defn roll-stats []
  (->> (repeatedly 6 #(r/roll-dice-command "3d6"))
       (map vector ["STR" "DEX" "CON" "INT" "WIS" "CHA"])))

(defn roll-background []
  (let [r (r/roll-dice-command "d20")]
    [(nth constants/backgrounds (dec (:value r))) r]))

(defn generate-attributes [sheet]
  (let [stats (roll-stats)]
    (reduce (fn [m [stat roll]]
              (assoc-in m
                        [:attributes (-> stat str/lower-case keyword)]
                        [(:value roll) roll]))
            sheet
            stats)))

(defn set-14-stat [sheet stat]
  (assoc sheet :selected-stat (stat->key stat)))

(defn do-development [background type]
  (let [dice (case type
               ::constants/growth "d6"
               ::constants/learning "d8")
        roll (r/roll-dice-command dice)
        development (get-in constants/background-development [background type])]
    [(nth development (dec (:value roll))) roll]))

(defn roll-background-development [background]
  {:free-skill (get-in constants/background-development [background ::constants/free-skill])
   :development (repeatedly 3 (fn [] (do-development background (rand-nth [::constants/growth ::constants/learning]))))})

(defn generate-background [sheet]
  (let [background (roll-background)
        development (roll-background-development (first background))]
    (assoc sheet
           :background background
           :development development)))

(defn generate-character []
  (-> {}
      generate-attributes
      generate-background))

(defn get-attribute [sheet attr]
  (-> (get-in sheet [:attributes (stat->key attr)])
      first))

(defn get-background [sheet]
  (-> sheet :background first name))

(defn get-development [sheet]
  (let [{free :free-skill
         dev  :development} (:development sheet)
        skills (->> dev
                    (map #(-> % first name))
                    (map (fn [x] ["DEVELOPMENT" x])))]
    (concat [["FREE SKILL" (name free)]] skills)))

(defn char->org-table [sheet]
  (let [attributes (mapv (fn [x] [x (get-attribute sheet x)]) ["STR" "DEX" "CON" "INT" "WIS" "CHA"])
        background [["BACKGROUND" (get-background sheet)]]
        development (get-development sheet)]
    (concat attributes background development)))
