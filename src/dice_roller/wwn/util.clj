(ns dice-roller.wwn.util
  (:require [dice-roller.roller :as r]
            [clojure.string :as str]))

(defn characterization-roll []
  (let [d4  (r/roll-dice-command "d4")
        d6  (r/roll-dice-command "d6")
        d8  (r/roll-dice-command "d8")
        d10 (r/roll-dice-command "d10")
        d12 (r/roll-dice-command "d12")
        d20 (r/roll-dice-command "d20")]
    (->> [d4 d6 d8 d10 d12 d20]
         (mapv :value))))

(defn clean-table [t-string]
  (->> (re-seq #"([0-9]+).?([0-9]+)?(.*)" t-string)
       (map #(into [] (drop 1 %)))))

(defn extract-characterization [[di-header table]]
  (let [[die-size label] (first di-header)]
    {:command die-size
     :label label
     :table (mapv (fn [[f s]] [f s]) table)}))

(comment
  (->> (slurp "./scratch/npc-types.txt")
       clean-table
       (map (fn [[s f t]]
              (let [k (if f
                        (str s "-" f)
                        s)]
                [k t])))
       (map (fn [[k v]] [k (re-seq #"[A-Z][^A-Z]+" v)]))
       (mapv (fn [[k v]]
              (let [[c b n] v]
                [k {:underclass c
                    :commoners  b
                    :gentry     n}])))
       )
  ;; items
  (->> c
       (mapv (fn [[s f t]]
              (let [k (if f
                        (str s "-" f)
                        s)]
                [k t]))) )

  ;; one-roll characterization
  (->> (slurp "./scratch/one-roll-characterization.txt")
       str/split-lines
       (remove empty?)
       (partition 2)
       (partition-by #(= \d (ffirst %)))
       (partition 2)
       (mapv extract-characterization))

  ;; elixirs
  (->> (slurp "./scratch/elixir-form.txt")
       str/split-lines
       (remove empty?)
       (partition 2)
       (partition-by #(= \d (ffirst %)))
       (partition 2)
       (mapv extract-characterization)
;
       )
  )
