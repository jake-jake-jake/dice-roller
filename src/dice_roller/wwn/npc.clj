(ns dice-roller.wwn.npc
  (:require
   [clojure.string :as str]
   [dice-roller.roller :as r]
   [dice-roller.util :as util]
   [dice-roller.wwn.constants :as constants]))

(defn roll-quick-npc []
  (merge (util/roll-on-table constants/npc-quick-type)
         (util/roll-on-table constants/npc-twist-table)))

(defn roll-npc-characterization []
  (into {} (for [table constants/npc-one-roll-characterization]
           (util/roll-on-table table))))

(defn ^::util/cli-fn roll-npc []
  (merge (roll-quick-npc)
         (roll-npc-characterization)))
