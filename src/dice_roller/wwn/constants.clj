(ns dice-roller.wwn.constants
  (:require [clojure.string :as str]
            [dice-roller.util :refer [range-table]]))

;; character creation
(def backgrounds
  [::artisan ::barbarian ::carter    ::courtesan ::criminal
   ::hunter  ::laborer   ::merchant  ::noble     ::nomad
   ::peasant ::performer ::physician ::priest    ::sailor
   ::scholar ::slave     ::soldier   ::thug      ::wanderer])

(def background-development
  {::artisan   {::growth    [::+1-any  ::+2-physical ::+2-physical ::+2-mental   ::exert    ::any-skill]
                ::learning  [::connect ::convince    ::craft       ::craft       ::exert    ::know      ::notice   ::trade]
                ::free-skill ::craft}
   ::barbarian {::growth    [::+1-any  ::+2-physical ::+2-physical ::+2-mental   ::exert    ::any-skill]
                ::learning  [::combat  ::connect     ::exert       ::lead        ::notice   ::punch     ::sneak    ::survive]
                ::free-skill ::survive}
   ::carter    {::growth    [::+1-any  ::+2-physical ::+2-physical ::+2-mental   ::connect  ::any-skill]
                ::learning  [::combat  ::connect     ::craft       ::exert       ::notice   ::ride      ::survive  ::trade]
                ::free-skill ::ride}
   ::courtesan {::growth    [::+1-any  ::+2-mental   ::+2-mental   ::+2-physical ::connect  ::any-skill]
                ::learning  [::combat  ::connect     ::convince    ::exert       ::notice   ::perform   ::survive  ::trade]
                ::free-skill ::perform}
   ::criminal  {::growth    [::+1-any  ::+2-mental   ::+2-physical ::+2-mental   ::connect  ::any-skill]
                ::learning  [::admin   ::combat      ::connect     ::convince    ::exert    ::notice    ::sneak    ::trade]
                ::free-skill ::sneak}
   ::hunter    {::growth    [::+1-any  ::+2-physical ::+2-physical ::+2-mental   ::exert    ::any-skill]
                ::learning  [::combat  ::exert       ::heal        ::notice      ::ride     ::shoot     ::sneak    ::survive]
                ::free-skill ::shoot}
   ::laborer   {::growth    [::+1-any  ::+1-any      ::+1-any      ::+1-any      ::exert    ::any-skill]
                ::learning  [::admin   ::any-skill   ::connect     ::convince    ::craft    ::exert     ::ride     ::work]
                ::free-skill ::work}
   ::merchant  {::growth    [::+1-any  ::+2-mental   ::+2-mental   ::+2-mental   ::connect  ::any-skill]
                ::learning  [::admin   ::combat      ::connect     ::convince    ::craft    ::know      ::notice   ::trade]
                ::free-skill ::trade}
   ::noble     {::growth    [::+1-any  ::+2-mental   ::+2-mental   ::+2-mental   ::connect  ::any-skill]
                ::learning  [::admin   ::combat      ::connect     ::convince    ::know     ::lead      ::notice   ::ride]
                ::free-skill ::lead}
   ::nomad     {::growth    [::+1-any  ::+2-physical ::+2-physical ::+2-mental   ::exert    ::any-skill]
                ::learning  [::combat  ::connect     ::exert       ::lead        ::notice   ::ride      ::survive  ::trade]
                ::free-skill ::ride}
   ::peasant   {::growth    [::+1-any  ::+2-physical ::+2-physical ::+2-physical ::exert    ::any-skill]
                ::learning  [::connect ::exert       ::craft       ::notice      ::sneak    ::survive   ::trade    ::work]
                ::free-skill ::exert}
   ::performer {::growth    [::+1-any  ::+2-mental   ::+2-physical ::+2-physical ::connect  ::any-skill]
                ::learning  [::combat  ::connect     ::exert       ::notice      ::perform  ::perform   ::sneak    ::convince]
                ::free-skill ::perform}
   ::physician {::growth    [::+1-any  ::+2-physical ::+2-mental   ::+2-mental   ::connect  ::any-skill]
                ::learning  [::admin   ::connect     ::craft       ::heal        ::know     ::notice    ::convince ::trade]
                ::free-skill ::heal}
   ::priest    {::growth    [::+1-any  ::+2-mental   ::+2-physical ::+2-mental   ::connect  ::any-skill]
                ::learning  [::admin   ::connect     ::know        ::lead        ::heal     ::convince  ::pray     ::pray]
                ::free-skill ::pray}
   ::sailor    {::growth    [::+1-any  ::+2-physical ::+2-physical ::+2-mental   ::exert    ::any-skill]
                ::learning  [::combat  ::connect     ::craft       ::exert       ::heal     ::notice    ::perform  ::sail]
                ::free-skill ::sail}
   ::scholar   {::growth    [::+1-any  ::+2-mental   ::+2-mental   ::+2-mental   ::connect  ::any-skill]
                ::learning  [::admin   ::heal        ::craft       ::know        ::notice   ::perform   ::pray     ::convince]
                ::free-skill ::sail}
   ::slave     {::growth    [::+1-any  ::+2-physical ::+2-physical ::+2-mental   ::exert    ::any-skill]
                ::learning  [::admin   ::combat      ::any-skill   ::convince    ::exert    ::sneak     ::survive  ::work]
                ::free-skill ::sneak}
   ::soldier   {::growth    [::+1-any  ::+2-physical ::+2-physical ::+2-physical ::exert    ::any-skill]
                ::learning  [::combat  ::combat      ::exert       ::lead        ::notice   ::ride      ::sneak    ::survive]
                ::free-skill ::combat}
   ::thug      {::growth    [::+1-any  ::+2-mental   ::+2-physical ::+2-physical ::connect  ::any-skill]
                ::learning  [::combat  ::combat      ::connect     ::convince    ::exert    ::notice    ::sneak    ::survive]
                ::free-skill ::combat}
   ::wanderer  {::growth    [::+1-any  ::+2-physical ::+2-physical ::+2-mental   ::exert    ::any-skill]
                ::learning  [::combat  ::connect     ::notice      ::perform     ::ride     ::sneak     ::survive  ::work]
                ::free-skill ::survive}})

;; npc characterization
(def npc-twist-table
  {:label "NPC Twists"
   :command "d12"
   :table [[1 "They’re abnormally rich or poor for their class"]
           [2 "They are extremely averse to using violence"]
           [3 "They tend to break their deals, wisely or not"]
           [4 "They have a reason to hate people like the PC"]
           [5 "They have a useful but unusual friend"]
           [6 "The PC or an ally owes them a debt"]
           [7 "A disaster is looming rapidly for them"]
           [8 "They have a reason to like people like the PC"]
           [9 "They’ve got few reliable local social ties"]
           [10 "They conceal a socially-lethal secret"]
           [11 "They’re an ex-convict or former underclass"]
           [12 "They have a secret identity"]]})

(def npc-quick-type
  {:command "d100"
   :label   "Quick NPC"
   :table   [["1-4"
              {:underclass "Black sheep of a gentry family",
               :commoners "Ambitious young entrepreneur",
               :gentry "Abbot or head of a monastery"}]
             ["5-8"
              {:underclass "Cast-off former gentry mistress",
               :commoners "Artisan of some local fame",
               :gentry "Court wizard or gentry mage"}]
             ["9-12"
              {:underclass "Coldly professional criminal",
               :commoners "Butler or servant to a major figure ",
               :gentry "Demihuman leader or high priest"}]
             ["13-16"
              {:underclass "Corrupt petty clerk or guard",
               :commoners "Fearfully respected local gossip",
               :gentry "Diplomat from a foreign land"}]
             ["17-20"
              {:underclass "Cynical dancing girl or boy",
               :commoners "Foreigner with enough money",
               :gentry "Discreet kingmaker or spymaster"}]
             ["21-24"
              {:underclass "Downfallen former gentry",
               :commoners "Harried landlord",
               :gentry "Ethnarch of a local minority"}]
             ["25-28"
              {:underclass "Foreigner struggling to survive",
               :commoners "Herbalist or local healer",
               :gentry "Feared semi-legit criminal boss"}]
             ["29-32"
              {:underclass "Gap-toothed local thug",
               :commoners "Local innkeeper or taverner",
               :gentry "Genteelly-kept noble hostage"}]
             ["33-36"
              {:underclass "Hardscrabble orphaned urchin",
               :commoners "Merchant in grain or bulk goods",
               :gentry "Kept pretender to a foreign title"}]
             ["37-40"
              {:underclass "Missionary of an unpopular god",
               :commoners "Merchant on his way down",
               :gentry "Local temple’s high priest"}]
             ["41-44"
              {:underclass "Penniless country boy or girl",
               :commoners "Military officer or guard official",
               :gentry "Magistrate or judge"}]
             ["45-48"
              {:underclass "Popular actor or actress",
               :commoners "Minor government functionary",
               :gentry "Major local official or minister"}]
             ["49-52"
              {:underclass "Prostitute with a heart of gold",
               :commoners "Modestly prosperous farmer",
               :gentry "Mayor or chief city official"}]
             ["53-56"
              {:underclass "Salt-worn common sailor",
               :commoners "Neighborhood shopkeeper",
               :gentry "Merchant-prince of wide affairs"}]
             ["57-60"
              {:underclass "Scabby beggar",
               :commoners "Obsessed local scholar",
               :gentry "Military general or high officer"}]
             ["61-64"
              {:underclass "Shabby boarding-house owner",
               :commoners "Peasant like all their neighbors are ",
               :gentry "Most popular courtesan in town"}]
             ["65-68"
              {:underclass "Shaky-handed slum physician",
               :commoners "Priest of a local faith",
               :gentry "Paid-off noble family disgrace"}]
             ["69-72"
              {:underclass "Slanderous poet or playwright",
               :commoners "Respected local ascetic or hermit",
               :gentry "Revered artistic genius"}]
             ["73-76"
              {:underclass "Thieving drunkard or junkie",
               :commoners "Retired adventurer",
               :gentry "Rusticating loser of a court feud"}]
             ["77-80"
              {:underclass "Tippling bargeman or boatman",
               :commoners "Scuffed but respectable laborer",
               :gentry "Ruthless noble clan patriarch"}]
             ["81-84"
              {:underclass "Unpopular but zealous artist",
               :commoners "Ship’s captain or officer",
               :gentry "Sorcerer of fearsome name"}]
             ["85-88"
              {:underclass "Unpopular demihuman",
               :commoners "Someone important’s mistress",
               :gentry "Spare prince or major noble scion"}]
             ["89-92"
              {:underclass "Weary ragpicker or scavenger",
               :commoners "Venerable family elder",
               :gentry "Traditional cultural office-holder"}]
             ["93-96"
              {:underclass "Widely-despised moneylender",
               :commoners "Veteran soldier or guardsman",
               :gentry "Wealthy but hated usurer"}]
             ["97-100"
              {:underclass "Worn-down day laborer",
               :commoners "Wandering peddler or trader",
               :gentry "Wealthy scholar-noble"}]]})

(def npc-one-roll-characterization
  [{:command "d4",
    :label "General Physical Build",
    :table
    [["1" "Unusually short or slender"]
     ["2" "Unremarkably average"]
     ["3" "Plump, voluptuous, or soft"]
     ["4" "Bulky, built, or more massive than usual"]]}
   {:command "d6",
    :label "The Way They Move",
    :table
    [["1" "Hesitant, fluttery, or delicately precise"]
     ["2" "Clumsy, with broad, careless motions"]
     ["3" "Slow and measured actions"]
     ["4" "Sharp, brisk, vigorous movements"]
     ["5" "Laborious or weary motions"]
     ["6" "Smooth, relaxed, efficient movement"]]}
   {:command "d8",
    :label "Clothing Idiosyncrasies",
    :table
    [["1" "A fondness for very bright or dull colors"]
     ["2" "Emblem of a faith or social group"]
     ["3" "Ill-kept, whether worn, dirty, or rumpled"]
     ["4" "Wears another group’s style of clothing"]
     ["5" "Abnormally risqué or modest for the society"]
     ["6" "Shows regular dirt and marks of their trade"]
     ["7" "Fastidiously neat or tastefully adorned"]
     ["8" "Wears a very noticeable piece of jewelry"]]}
   {:command "d10",
    :label "The First Thing Noticed",
    :table
    [["1" "Their voice is unusual and arresting"]
     ["2" "They’re disfigured or missing a limb"]
     ["3" "They have a distinctive scent or stench"]
     ["4" "Their clothing is very unusual"]
     ["5" "They have distinct mannerisms of gesture"]
     ["6" "Their accent is unusual for the location"]
     ["7" "They’re abnormally friendly or reserved"]
     ["8" "They have signs of a chronic illness or wound"]
     ["9" "They have an unusual hair style or texture"]
     ["10" "They are very fat, thin, short, tall or such"]]}
   {:command "d12",
    :label "One Way They Differ From Expectations",
    :table
    [["1" "Unusual skin coloration or texture"]
     ["2" "From a group not normally found here"]
     ["3" "Much older or younger than expected"]
     ["4" "Abnormal temperament for their trade"]
     ["5" "Overt devotee of an uncommon faith here"]
     ["6" "A pet animal not normally found here"]
     ["7" "They eat a cuisine that is not liked here"]
     ["8" "Abnormal hair texture or hue"]
     ["9" "They wear unusual tattoos or body jewelry"]
     ["10" "They’re uncommonly socially awkward"]
     ["11" "A physical build abnormal for the area"]
     ["12" "A distinct mutation or body alternation"]]}
   {:command "d20",
    :label "Visible Mannerisms or Traits",
    :table
    [["1" "An often-used catchphrase or verbal tic"]
     ["2" "Always drinking or near a drink"]
     ["3" "Hands never stop moving"]
     ["4" "Talks very slowly and deliberately"]
     ["5" "Tends to shift their weight constantly"]
     ["6" "Has an unusual hairstyle or beard"]
     ["7" "Appears often to be slightly drugged or drunk"]
     ["8" "Prefers to always be in shade if possible"]
     ["9" "Asks far too many questions"]
     ["10" "Never looks you in the eye when lying"]
     ["11" "Visibly avaricious for gold, food, or such else"]
     ["12" "Always circuitous about answering questions"]
     ["13" "Never directly denies or refuses anything"]
     ["14" "Paces constantly; always prefers to stand"]
     ["15" "Has work-stained or scarred hands"]
     ["16" "Old pox marks or disease scars are present"]
     ["17" "Prefers elaborate hats, belts, or adornments"]
     ["18" "Always toying with worry beads or such"]
     ["19" "Knocks things over carelessly"]
     ["20" "Checks written notes regularly"]]}])

;; items
(def magic-items
  {:command "d100"
   :label   "Magic Item"
   :table   [["1-2"   "Guncloud"]
             ["3-6"   "Censer of Solid Dreams"]
             ["7-12"  "Congealed Paradox"]
             ["13-16" "Enough Rope"]
             ["17-19" "Eye of Discerning Humanity"]
             ["20-23" "Flask of Devils"]
             ["24-26" "Ghost Horse"]
             ["27-31" "Immanent Beacon Shard"]
             ["32-35" "Implacable Fist"]
             ["36-39" "Jewel Beyond Price"]
             ["40-42" "Key of the Former House"]
             ["43-46" "Lens of the Polychrome Lord"]
             ["47-49" "Lex Talionis"]
             ["50"    "Logos"]
             ["51-53" "Lyre of Longing"]
             ["54-57" "Memorious Fragrance"]
             ["58-62" "Purification Salt"]
             ["63-65" "Rain-Beckoning Drum"]
             ["66-69" "Ring of the Sea Princes"]
             ["70-72" "Rod of Repudiation"]
             ["73-77" "Seed of Flesh"]
             ["78-80" "Staff of the Old Lords"]
             ["81-84" "Stonepeg"]
             ["85-87" "Stutterhatchet"]
             ["88-91" "Vessel of Hours"]
             ["92-95" "Vigilant Banner"]
             ["96-00" "Vothite Icon"]]})

(def elixir-type
  {:command "d100",
   :label "Elixir",
   :table
   [["1-4" "Anchoring Draught"]
    ["5–8" "Bestial Form"]
    ["9–12" "Blood of Boiling Rage"]
    ["13–15" "Bodily Innervation"]
    ["16–20" "Borrowed Flesh"]
    ["21–24" "Cold Courage"]
    ["25–28" "Congealed Winter"]
    ["29–32" "Deep Sight"]
    ["33–38" "Energetic Impermeability"]
    ["39–42" "Inverted Entropy"]
    ["43–46" "Murderous Anointing"]
    ["47" "Nectar of Immortality"]
    ["48–49" "Nepenthe"]
    ["50–53" "Omened Visions"]
    ["54–58" "Persistent Luminescence"]
    ["59–62" "Plasmic Molding"]
    ["63–64" "Quintessence of the Hour"]
    ["65–68" "Sacrificial Strength"]
    ["69–76" "Sanctified Healing"]
    ["77–79" "Scalding Breath"]
    ["80–83" "Soul Sight"]
    ["84–85" "Thaumic Vitality"]
    ["86–90" "The Great and the Small"]
    ["91–94" "Truthspeaking"]
    ["95–100" "Wrathful Detonation"]]})

(def elixir-appearance
  [{:command "d10",
    :label "The Elixir’s Form",
    :table
    [["1" "Bottled vapor"]
     ["2" "Breakable token"]
     ["3" "Incense stick"]
     ["4" "Inhaler"]
     ["5" "Injector"]
     ["6" "Inked charm"]
     ["7" "Jarred salve"]
     ["8" "Liquid potion"]
     ["9" "Pill"]
     ["10" "Preserved edible"]]}
   {:command "d10",
    :label "The Elixir’s Quirks of Appearance",
    :table
    [["1" "Vividly unnatural color"]
     ["2" "Pungent or appealing aroma"]
     ["3" "Strange texture or consistency"]
     ["4" "Metallic or glowing appearance"]
     ["5" "It moves slightly of its own"]
     ["6" "Unnaturally heavy or light"]
     ["7" "Makes a very soft sound"]
     ["8" "Bears imprinted text"]
     ["9" "Abnormally hot or chilled"]
     ["10" "Changes shape regularly"]]}])
