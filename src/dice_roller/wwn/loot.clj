(ns dice-roller.wwn.loot
  (:require
   [dice-roller.wwn.constants :as constants]
   [dice-roller.util :as util]
   [clojure.string :as str]
   [dice-roller.roller :as r]))

(defn ^::util/cli-fn magic-item []
  (util/roll-on-table constants/magic-items))

(defn ^::util/cli-fn roll-elixir []
  (merge (util/roll-on-table constants/elixir-type)
         (into {} (for [table constants/elixir-appearance]
                    (util/roll-on-table table)))))
