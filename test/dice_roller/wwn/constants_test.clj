(ns dice-roller.wwn.constants-test
  (:require [dice-roller.wwn.constants :as sut]
            [clojure.test :refer :all]))

(def test-table-a
  [["1-3" :type-a]
   ["4"   :type-b]])

(def test-table-b
  [["1"   :type-a]
   ["2-3" :type-b]
   ["4"   :type-c]])

(deftest range-table-test
  (is (= {1 :type-a, 2 :type-a, 3 :type-a, 4 :type-b}
         (sut/range-table test-table-a)))
  (is (= {1 :type-a, 2 :type-b, 3 :type-b, 4 :type-c}
         (sut/range-table test-table-b))))
