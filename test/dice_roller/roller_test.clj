(ns dice-roller.roller-test
  (:require [dice-roller.roller :as sut]
            [clojure.test :as t]))

(t/deftest roll-dice-command-test
  (t/testing "multiple dice with modifiers"
    (let [roll-result (sut/roll-dice-command "\n2d20 +10\n")]
      (t/is (= 10
               (:modifier roll-result)))
      (t/is (= 2
               (count (:rolls roll-result))))
      (t/is (= "2d20 +10"
               (:roll-command roll-result)))
      (t/is (= (+ (apply + (conj (:rolls roll-result) (:modifier roll-result))))
               (:value roll-result)))))
  (t/testing "single dice no modifier"
    (let [roll-result (sut/roll-dice-command "1d6")]
      (t/is (= 0
               (:modifier roll-result)))
      (t/is (= 1
               (count (:rolls roll-result))))
      (t/is (= "1d6"
               (:roll-command roll-result)))
      (t/is (= (+ (apply + (conj (:rolls roll-result) (:modifier roll-result))))
               (:value roll-result)))))
  (t/testing "single dice negative modifier"
    (let [roll-result (sut/roll-dice-command "1d6 -2")]
      (t/is (= -2
               (:modifier roll-result)))
      (t/is (= 1
               (count (:rolls roll-result))))
      (t/is (= "1d6 -2"
               (:roll-command roll-result)))
      (t/is (= (+ (apply + (conj (:rolls roll-result) (:modifier roll-result))))
               (:value roll-result)))))
  (t/testing "bad dice command"
    (let [roll-result (sut/roll-dice-command "a bad roll command")]
      (t/is (nil? (:roll-command roll-result))))))
