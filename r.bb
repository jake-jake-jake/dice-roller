#!/usr/bin/env bb

(ns r
  (:require
   [dice-roller.roller :as r]
   ))

(prn (r/roll-dice-command (apply str *command-line-args*)))
